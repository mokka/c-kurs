#include <string.h>
#include <ctype.h>

int strclean(char* t);

int strclean(char* t) {
    int length = strlen(t), removedChars = 0;

    for (char c = *t; c != '\0'; c = *t) {

        if ((!isprint(c)) || (isspace(c))) {
            strcpy(t, t+1);
            removedChars++;
        }
        t++;
    }

    return removedChars;
}