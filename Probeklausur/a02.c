int sequence(int x, int n);

int sequence(int x, int n) {
    
    /* Falls n < 0 ist... */
    if (n < 0) return 0;
    
    /* Falls n > 0 ist... */
    if (n > 0) {

        /* Wert von x_(n-1) */
        int xmn = sequence(x, n - 1);

        /* Falls x_(n-1) = 1 ist.. */
        if (xmn == 1) return 1;

        /* Falls x_(n-1) ungerade ist... */
        if (xmn % 2 == 1) return (3 * xmn + 1);

        /* Falls x_(n-1) gerade ist... */        
        else return (xmn / 2);
    }

    /* Falls n = 0 ist...*/
    return x;
}