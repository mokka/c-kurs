#include <string.h>

int lex_sorted(const char* array[]);

int lex_sorted(const char* array[]) {

    /* Keine Elemente: nicht lexikographisch aufsteigend sortiert,
    weil keine Elemente vorhanden. */
    if (array[0] == NULL) return 0;

    /* Ein Element: lexikographisch aufsteigend sortiert,
    weil nur ein Element vorhanden. */
    if (array[1] == NULL) return 1;

    for (int i = 0; array[i+1] != NULL; i++) {
        if (strcmp(array[i], array[i+1]) > 0) return 0;
    }

    return 1;
}