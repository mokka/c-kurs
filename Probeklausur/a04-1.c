#include <string.h>     /* strcpy   */
#include <stdlib.h>     /* malloc   */
#include <limits.h>     /* CHAR_MAX */
#include "a04-testing.h"

data_t* new_data(char* name, int number);


data_t* new_data(char* name, int number) {
    data_t* dt_ptr = (data_t*) malloc(sizeof(data_t));

    strcpy(dt_ptr->name, name);
    dt_ptr->number = number;

    int cksum = 0;
    for (int i = 0; i < 32; i++) {

        if (dt_ptr->name[i] == '\0')
            break;

        cksum += dt_ptr->name[i];
        cksum = cksum % CHAR_MAX;
    }

    cksum += dt_ptr->number;
    cksum = cksum % CHAR_MAX;

    dt_ptr->checksum = cksum;

    return dt_ptr;
}