#include <stdio.h>

#include "a04-testing.h"

int main(void) {

    data_t* ptr_saturn = new_data("Saturn", 5);

    printf("Datensatz:\n\tName:\t\t%s\n\tNummer:\t\t%d\n\tPruefsumme:\t%d",
        ptr_saturn->name, ptr_saturn->number, ptr_saturn->checksum
    );

    return 0;
}