
@echo off
set file=%1
set src=%file%.c
set filef=%file%-testing
set srcf=%filef%.c

gcc %srcf% -c -Wall -std=c11
gcc %src%  -c -Wall -std=c11

move *.o .\bin
echo ~
echo ~

gcc -o bin\%file%.exe bin\%filef%.o bin\%file%.o