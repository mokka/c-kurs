# C-Kurs

Materialien, hauptsächlich gelöste Aufgaben des SSS, für den C-Programmierkurs im WiSe 2017/18.

## Lösungen

| Aufgabe   | SSS % | Link    | Notes    |
| --------- | ----- |:-------:| -------------- |
|Hello World|100    | [Click!](hello_world.c) |     |
|S4         |100    | [Click!](s4.c) |      |
|S5         |100    | [Click!](s5.c) |      |
|S6         |100    | [Click!](s6.c) |      |
|S7         |100    | [Click!](s7.c) |      |
|S8         |100    | [Click!](s8.c) |      |
|S10        |100    | [Click!](s10.c) |      |
|E3         |100    | [Click!](e3.c) |      |
|S9         |100    | [Click!](s9.c) | Now uses selection sort, still working at 100%     |
|E5         |100    | [Click!](e5.c) |      |
|Z1         |100    | [Click!](z1.c) |      |
|S12        |100    | [Click!](s12.c) |      |
|Z2         |100    | [Click!](z2.c) |      |
|S14        |100    | [Click!](s14.c) | Both Eratosthenes and Divisor Search are implemented and working properly.     |
|S20        |100    | [Click!](s20.c) |      |
|E21        |100    | [Click!](e21.c) |      |
|S24        |100    | [Click!](s24.c) | Functions strend, strrchr and strstr work correctly, using only pointer airthmetics.     |
|Z3         |100    | [Click!](z3.c) |      |
|Z4         |100    | [Click!](s24.c) | Solution to Z3 is verbatim to the solution for S24. That solution already pure pointer arithmetics.     |
|Z7         |100    | [Click!](z7.c) |      |
|E22        |100    | [Click!](e22.c) |      |
|E23        |100    | [Click!](e23.c) |      |
|Z8         |100    | [Click!](z8.c) |      |
|Z5         |100    | [Click!](z5.c) |      |
|Z6         |100    | [Click!](z6.c) |      |
|S15        |100    | [Click!](s15.c) |      |
|S17        |100    | [Click!](s17.c) |      |
|S21        |100    | [Click!](s21.c) |      |
|E16        |100    | [Click!](e16.c) |      |
|E29        |100    | [Click!](e29.c) |      |
|Total      |100    |     -      |      |