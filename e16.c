/**
 * Prints out the actual physical amount of storage
 * needed for various integer data types, using
 * the sizeof operator.
**/

#include <stdio.h>

int main(void) {

    printf(
        "char: %llu\nshort: %llu\nint: %llu\nlong: %llu\n",
        sizeof(char),
        sizeof(short),
        sizeof(int),
        sizeof(long)
    );
    
    return 0;
}
