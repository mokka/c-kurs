/**
 * Test, if given array p with length laenge
 * is a permutation, or not.
**/

#include <stdio.h>
#include <stdbool.h>	/* bool		*/
#include <string.h> 	/* memset	*/

/*
Returns 1 if the given array of length n is a permutation
of the numbers 1...length, and returns 0 if not.
*/

int permtest(const unsigned int* p, const int laenge);

int permtest(const unsigned int* p, const int laenge) {

	bool check[laenge];
	memset(check, false, sizeof(check));

	if (laenge == 0)
		return 0;

	for (int i = 0; i < laenge; i++) {

		if (p[i] <= 0 || p[i] > laenge)
			return 0;

		if (check[p[i] - 1] == true)
			return 0;
			
		check[p[i] - 1] = true;
	}

	for (int i = 0; i < laenge; i++) {
		if (check[i] == false)
			return 0;
	}

	return 1;
}
