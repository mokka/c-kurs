/**
 * Implements the function permatcheck.
**/

/*
Returns the value at given location; util.
*/

int valueAt(const int** p, int row, int column);

/*
Checks whether given 2-dimensional int array
is a permutation matrix or not.
*/

int permatcheck(const int** p, int laenge);


int permatcheck(const int** p, int laenge) {

	/*
	Iterate through all rows, check by column,
	if there's a 1 or 0.
    */

	for (int row = 0; row < laenge; row++) {


		/*
		Number of 1s counted in current row.
		*/
		
		int oneCountRow = 0;

		/*
		Iterate through the columns.
		*/

		for (int col = 0; col < laenge; col++) {

			int val = valueAt(p, row, col);

			if (val != 0 && val != 1)
				return 0;

			if (val == 1) {

				if (oneCountRow > 0)
					return 0;
				else {

					oneCountRow++;

					/*
					Iterate through the rows on that column,
					to see if there's another 1 somewhere in the
					remaining columns down below.
					*/

					for (int row2 = row + 1; row2 < laenge; row2++) {

						val = valueAt(p, row2, col);

						if (val != 0 && val != 1)
							return 0;

						if (val == 1)
							return 0;
					}
				}
			}
		}

		if (oneCountRow != 1)
			return 0;
	}
	return 1;
}


int valueAt(const int** p, int row, int column) {

    return ( *(*(p + row) + column) );
}
