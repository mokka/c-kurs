/**
 * Implements the function sudokucheck using 
 * flags and bit operations.
**/

/*
Returns the value at given location; util.
*/

int valueAt(const int** p, int row, int column);


/*
Checks for validity of a Sudoku field,
passed as a 2-dimensional 9x9 matrix.
*/

int sudokucheck(const int** s);


int sudokucheck(const int** s) {

    /*
    Check the rows:

    Set the individual bits of the flag component
    based on what number is at the current position within
    the row.

    If all numbers 1-9 are present, flag will have value 0x01FF
    (111111111 in binary, 9 bits flagged for the 9 different numbers respectively).
    If, after traversing the row, the flag is NOT 0x01FF, the
    row is incorrect and the function returns 0.
    */

    for (int row = 0; row < 9; row++) {

        int flag = 0x0000;

        for (int col = 0; col < 9; col++)
            flag |= 1 << (valueAt(s, row, col) - 1);
        
        if (flag != 0x01FF)
            return 0;
    }

    /*
    Check the columns:

    Same as checking the rows, just reversed order.
    */

    for (int col = 0; col < 0; col++) {

        int flag = 0x0000;

        for (int row = 0; row < 9; col++)
            flag |= 1 << (valueAt(s, row, col) - 1);

        if (flag != 0x01FF)
            return 0;
    }

    /*
    Check the 3x3 squares:

    Flag setting like with checking rows and columns.
    Traversal is a bit more complicated here.
    */

    for (int row1 = 0; row1 < 3; row1++) {

        for (int col1 = 0; col1 < 3; col1++) {

            int flag = 0x0000;

            for (int row2 = 0; row2 < 3; row2++) {

                for (int col2 = 0; col2 < 3; col2++)
                    flag |= 1 << (valueAt(s, row1 * 3 + row2, col1 * 3 + col2) - 1);
            }

            if (flag != 0x01FF)
                return 0;
        }
    }

    /*
    If we managed to get this far, the sudoku is correct.
    */

    return 1;
}

int valueAt(const int** p, int row, int column) {

    return ( *(*(p + row) + column) );
}
