/**
* Implements the date conversion from s20/s21, but respects the year
* of the date as well.
**/

/*
Return 1 if given year is a leap year, return 0 otherwise.
*/

int schaltjahr(int jahr);

/*
Convert a date consisting of day of month and
month the ordinal number of the day in the year.

Returns that day.
*/

int datum2int(int tag, int monat, int jahr);

/*
Write the day of month/month date based on
the given ordinal number of the day in the year
to the array "ergebnis".
*/

void int2datum(int tage, int jahr, int* ergebnis);

/*
Day offsets for all the months.
First subarray contains offsets for non-leap years,
second subarray contains offsets for leap years.
*/

const int offset[2][12] = {
	{ 0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334 },
	{ 0, 31, 60, 91, 121, 152, 182, 213, 244, 274, 305, 335 }
};

void int2datum(int tage, int jahr, int* ergebnis) {

	int schalt = schaltjahr(jahr);

	if (tage <= 0)
		return int2datum(tage + (365 + schaltjahr(jahr - 1)), jahr - 1, ergebnis);

	if (tage > (365 + schalt))
		return int2datum(tage - (365 + schalt), jahr + 1, ergebnis);


	for (int i = 11; i >= 0; i--) {

		int diff = tage - offset[schalt][i];

		if (diff > 0) {
			ergebnis[0] = diff;
			ergebnis[1] = i + 1;
			ergebnis[2] = jahr;
			break;
		}
	}
}

int datum2int(int tag, int monat, int jahr) {

	return offset[schaltjahr(jahr)][monat - 1] + tag;
}

int schaltjahr(int jahr) {

	/*
	All four years is a leap year, except for every 100th year,
	but the 400th year is a leap year again.
	*/

	return (jahr % 4 == 0 && jahr % 100 != 0) || (jahr % 400 == 0);
}
