/**
 * Print out a text with escaped characters to the StdOut.
**/

#include <stdio.h>

int main(void) {

    printf("Er kam lässig heran und sagte nur \"Na, wie geht\'s?\".\nKommentare beginnen mit /* und enden mit */. Verwechseln Sie\ndas bitte nicht mit \\* bzw. *\\!");
    return 0;
}
