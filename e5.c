/**
 * Read in the coefficients for a polynomial from the StdIn.
 * Solve this polynomial based on values read in from the StdIn,
 * by simplifying the polynomial using Horner's method.
**/

#include <stdio.h>

/*
Recursively obtain the value of the polynomial of the deegree
n at position x using Horner's method. 
*/

double horner(double coef[], double x, int n);

/*
Reverse an array; utility
*/

void reverse(double* array, int length);

int main(void) {

	double koeffizienten[33];
	double tmp;
	int grad = 0;
	int res;

    /*
    Read in the coefficients from the StdIn, until either
    user signals EOF, or 32 coefficients collected.
    */

	for (; grad <= 32; grad++) {

		res = scanf("%lf", &tmp);

		if (res == 0) {
			printf("Fehler bei der Eingabe.\n");
			return 255;
		}

		if (res == EOF)
			break;

		koeffizienten[grad] = tmp;
	}

    /*
    Reverse array of collected coefficients for convenience.
    */

    reverse(koeffizienten, grad-1);

	printf("Bitte Stellen zur Auswertung angeben\n");

    /*
    Read in and calculate the values of the
    polynomial at given values for x.
    */

	while (1) {

		res = scanf("%lf", &tmp);

		if (res == 0) {
			printf("Fehler bei der Eingabe.\n");
			return 255;
		}

		if (res == EOF)
			break;

		printf("Wert des Polynoms an der Stelle %g: %g\n", tmp, horner(koeffizienten, tmp, grad - 1));
	}

	return 0;
}

double horner(double coef[], double x, int n) {

	double q;
	if (n > 0)
		q = horner(coef, x, n - 1);
	else
		return coef[n];

	return q * x + coef[n];
}

void reverse(double* array, int length) {

	int start = 0;

    while (start < length) {
        int tmp = array[start];
        array[start++] = array[length];
        array[length--] = tmp;
    }
}
