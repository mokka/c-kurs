/**
 * Print out "Hello World!" and a newline.
**/

#include <stdio.h>

int main() {
    printf("Hello World!\n");
    return 0;
}
