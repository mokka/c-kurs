/**
 * Reads in two strings with up to 50 characters each,
 * concatenates them and outputs the concatted string to StdOut.
**/

#include <stdio.h>


/*
Read in a string with the maximum length "length",
ending at the "end" char and store the string in buffer.
*/

void readString(char* buffer, int length, char end);

int main(void) {

    char strng1[50];
    char strng2[50];

    readString(strng1, 49, '\n');
    readString(strng2, 49, '\n');

    printf("%s%s\n", strng1, strng2);

    return 0;
}

void readString(char* buffer, int length, char end) {

    int count = -1;

    for (char c = 0; c != end; c = getchar()) {
        
        if (count == length && c != end) {
            printf("Fehler bei der Eingabe.\n");
            return;
        }
        
        buffer[count] = c;
        count++;
    }

    buffer[count] = '\0';
}
