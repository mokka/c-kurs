/**
 * Read in a string from the StdIn and count the occurences
 * of the read-in characters.
 * Print of the number of occurences of all the chars.
**/

#include <stdio.h>
#include <stdlib.h> /* memset   */
#include <string.h> /* EOF      */
#include <ctype.h>  /* isprint  */

int main(void) {

    int count[128];
    memset(count, 0, 128*sizeof(int));
    
    for (char c = getchar(); c != EOF; c = getchar()) {
        count[(int) c]++;
    }

    for (int i = 0; i < 128; i++) {
        if (isprint(i))
            printf("%c : %d\n", i, count[i]);
        else
            printf("<CTRL> %d : %d\n", i, count[i]);
    }
    return 0;
}