/**
 * Calculate and print out the prime numbers
 * less than 1000 using two different methods:
 * 
 * 1. Bruteforce by divisor checking
 * 2. Eratosthenes' method
**/

#include <stdio.h>
#include <stdbool.h>    /* bool     */
#include <string.h>     /* memset   */
#include <math.h>       /* sqrt     */

/*
Implementation of Eratosthenes' method to print
out all prime numbers between 1 and n.
*/

void Eratosthenes(const int n);

/*
Print out all prime numbers between 1 and n
using divisor search up to the square root of n.
*/

void RootFind(const int n);

int main(void) {

    Eratosthenes(1000);
    printf("\n");
    // RootFind(1000);
    // printf("\n");
        
    return 0;
}

void Eratosthenes(const int n) {

    /*
    Declare a boolean array to store prime
    information on the numbers 1...n.

    Initialize the entire array (all values)
    with true.
    */

    bool prime[n+1];
    memset(prime, true, sizeof(prime));

    /*
    Iterate through the "uncertain" prime numbers
    from 2...n (1 is always prime by definition).

    If the currently observed number p is a prime number,
    set all of its multiples to "not prime", i.e to false.
    */

    for (int p = 2; (p * p) <= n; p++)
        if (prime[p] == true)
            for (int i = p * 2; i <= n; i += p)
                prime[i] = false;

    /*
    Iterate through the prime array and print
    out all numbers marked to be prime.
    */

    for (int p = 2; p <= n; p++)
        if (prime[p])
            printf("%d ", p);
}

void RootFind(const int n) {
    
    /*
    Declare a boolean array to store prime
    information on the numbers 1...n.

    Initialize the entire array (all values)
    with true.
    */

    bool prime[n+1];
    memset(prime, true, sizeof(prime));

    int c;

    for (int a = 2; a <= n; a++)
        for (c = 2; c <= (int)(sqrt(a)); c++)
            if (a%c == 0)
                prime[a] = false;


    /*
    Iterate through the prime array and print
    out all numbers marked to be prime.
    */

    for (int p = 2; p <= n; p++)
        if (prime[p])
            printf("%d ", p);
}
