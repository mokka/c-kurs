/**
 * Implements the recursive functin ggT, which calculates
 * the greatest common divisor (GCD) of two given numbers a and b.
**/

unsigned long ggT(unsigned long a, unsigned long b);

unsigned long ggT(unsigned long a, unsigned long b) {

    if (b == 0)
        return a;

    if (b > 0)
        return ggT(b, a % b);

    return 0;
}
