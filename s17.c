/**
 * Return 1 if given year is a leap year, and return 0 if not.
**/

int schaltjahr(int jahr);

int schaltjahr(int jahr) {

    /*
    All four years is a leap year, except for every 100th year,
    but the 400th year is a leap year again.
    */

    return (jahr % 4 == 0 && jahr % 100 != 0) || (jahr % 400 == 0);
}
