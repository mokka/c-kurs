/**
 * Convert a date (day/month) to the ordinal
 * number of the day in the year; or vice versa.
**/

#include <stdio.h>

/*
Convert a date consisting of day of month and
month the ordinal number of the day in the year.

Returns that day.
*/

int datum2int(int tag, int monat);

/*
Write the day of month/month date based on
the given ordinal number of the day
to the array "ergebnis".
*/

void int2datum(int tage, int* ergebnis);

/*
Day offsets for all the months.
*/

const int offset[12] = {
	0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334
};


int datum2int(int tag, int monat) {

    return offset[monat-1] + tag;
}


void int2datum(int tage, int* ergebnis) {

    for (int i = 11; i >= 0; i--) {

        int diff = tage - offset[i];

        if (diff > 0) {
            ergebnis[0] = diff;
            ergebnis[1] = i+1;
            break;
        }
    }
}
