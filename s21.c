/**
 * Convert a date (day/month) to the ordinal
 * number of the day in the year; or vice versa.
 * 
 * Much like S20, but also accepts "exotic" inputs.
**/

/*
Write the day of month/month date based on
the given ordinal number of the day in the year
to the array "ergebnis".
*/

void int2datum(int tage, int* ergebnis);


void int2datum(int tage, int* ergebnis) {

    if (tage <= 0)
        return int2datum(tage + 365, ergebnis);

    if (tage > 365)
        return int2datum(tage - 365, ergebnis);

    static const int offset[12] = {
        0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334
    };

    for (int i = 11; i >= 0; i--) {

        int diff = tage - offset[i];

        if (diff > 0) {
            ergebnis[0] = diff;
            ergebnis[1] = i+1;
            break;
        }
    }
}
