/**
 * Implements the functions strend, strrchr and strstr
 * for various string manipulations.
**/

#include <stdio.h>

/*
Returns the last character of given string.
*/

char *getLastChar(char *input);

/*
Returns 1 if the string t is at the end of string s, and 0 otherwise.
*/

int strend (const char *s, const char *t);

/*
Returns a pointer to the last occurence of the char c in the
string s, returns the NULL pointer if c does not occur in s at all.
*/

char *strrchr (const char *s, int c);

/*
Returns a pointer to the first occurence of the string t in string s,
returns the NULL pointer if t does not occur in s.
*/

char *strstr (const char *s, const char *t);


char *getLastChar(char *input) {
    while(*input)
        input++;
    return input;
}

int strend (const char *s, const char *t) {

    if (strlen(s) < strlen(t))
        return 0;

    char* tEnd = getLastChar(t);
    char* sEnd = getLastChar(s);

    char* curtChar = tEnd;
    char* cursChar = sEnd;

    while (cursChar >= s && curtChar >= t) {

        if (*cursChar != *curtChar) {
            return 0;
        }

        curtChar--;
        cursChar--;
    }

    return 1;
}

char *strrchr (const char *s, int c) {

    char* save;
    char ch;

    for (save = (char*) 0; (ch = *s); s++) {

        if (ch == c)
            save = (char*) s;
    }

    return save;
}

char *strstr (const char *s, const char *t) {

    char* a,* b;

    b = t;

    if(*b == 0) {
        return s;
    }

    for ( ; *s != 0; s += 1) {
	if (*s != *b) {
	    continue;
	}
	a = s;
	while (1) {
	    if (*b == 0)
            return s;

	    if (*a++ != *b++)
            break;
            
	}
	b = t;
    }
    return NULL;
}
