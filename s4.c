/**
 * Print out min/max values for different integer
 * and float data types in C.
**/

#include <limits.h>
#include <float.h>
#include <stdio.h>

int main (void) {

    printf("CHAR_MIN: %i\n", CHAR_MIN);
    printf("CHAR_MAX: %i\n", CHAR_MAX);
    printf("INT_MIN: %i\n", INT_MIN);
    printf("INT_MAX: %i\n", INT_MAX);
    printf("FLT_MIN: %1.5e\n", FLT_MIN);
    printf("FLT_MAX: %g\n", FLT_MAX);
    printf("DBL_MIN: %1.5e\n", DBL_MIN);
    printf("DBL_MAX: %g\n", DBL_MAX);
    return 0;
}
