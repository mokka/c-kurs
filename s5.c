/**
 * Print out all chars between 'A' and 'z'.
**/

#include <stdio.h>

int main (void) {

    for (char i = 'A'; i <= 'z'; i++)
        printf("%c",i);

    printf("\n");

    return 0;
}
