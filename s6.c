/**
 * Read in integers through StdIn and print our their
 * octal and hexadecimal values, until user signals EOF
 * or an invalid input is read.
**/

#include <stdio.h>

int main() {

    int ganzZahl;
    int scanResult;

    while (1) {

        scanResult = scanf("%d", &ganzZahl);

        if (scanResult == 0 || ganzZahl < 0)
            return 255;

        printf("OKT: %o HEX: %x\n", ganzZahl, ganzZahl);
    }

    return 0;
}
