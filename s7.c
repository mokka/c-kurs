/**
 * Calculate the digit sum of a positive integer read in through StdIn.
**/

#include <stdio.h>

int main(void) {

    int n, quers = 0, check = scanf("%d", &n);

    if (check == 0 || n < 0) {
        printf("Fehler bei der Eingabe.\n");
        return 255;
    }

    while (n > 0) {
        quers += n % 10;
        n /= 10;
    }

    printf ("Quersumme: %d", quers);

    return 0;
}
