/**
 * Read in numbers from the standard input and calculate
 * the arithmetic mean of these numbers.
**/

#include <stdio.h>

int main(void) {


    int res = 0, mittelC = 0;
    double mittelW = 0, zahl = 0;
    
   do {

        res = scanf("%lf", &zahl);

        if (res == 0) {
            printf("Fehler bei der Eingabe.\n");
            return 255;
        }
        
        if (zahl == EOF) {
            break;
        }

        mittelW += zahl;
        mittelC++;

    }  while (1);

    printf("Lösung: %lf\n", (mittelW) / ((double) mittelC));
    return 0;
}
