/**
 * Read in up to 100 integers (or until EOF signalled),
 * print out these collected numbers, sort the numbers
 * in ascending order and print them out again.
**/

#include <stdio.h>

/*
Print out an array; utility.
*/

void printFeld(int size, int feld[]);

/*
Sort the array storing the collected integers using
selection sort.
*/

void sortFeld(int size, int feld[]);

/*
Swap array elements; util.
*/

void swap(int *left, int *right);


int main(void) {

    int tmp, res = 0, count = 0;
    int feld[100];

    printf("Bitte bis zu 100 ganze Zahlen eingeben:\n");

    /*
    Read in up to 100 integer values from the StdIn.
    */

    for (; count < 100; count++) {

        res = scanf("%d", &tmp);

        if (res == 0) {
            printf("Fehler bei der Eingabe.\n");
            return 255;
        }

        if (res == EOF)
            break;


        feld[count] = tmp;    
    }

    printf("Die zu sortierenden Zahlen: ");
    printFeld(count, feld);

    sortFeld(count, feld);

    printf("Die Zahlen sortiert: ");
    printFeld(count, feld);
}

void printFeld(int size, int feld[]) {

    for (int i = 0; i < size; i++)
        printf("%d ", feld[i]);

    printf("\n");
}

void sortFeld(int size, int feld[]) {

    int min = 0;

    for (int i = 0; i < size - 1; i++) {

        min = i;
        for (int j = i + 1; j < size; j++)
            if (feld[j] < feld[min])
                min = j;

        swap(&feld[min], &feld[i]);
    }
}

void swap(int *left, int *right) {
    int tmp = *left;
    *left = *right;
    *right = tmp;
}
