/**
 * Read in a binary number for StdIn, convert it to 
 * decimal format and print it out again.
**/

#include <stdio.h>

int main(void) {

    int decimal = 0, res, rest, base = 1;
    long binary;

    res = scanf("%ld", &binary);

    if (res == 0 || res == EOF) {
        printf("Fehler bei der Eingabe.\n");
        return 255;
    }

    /*
    Convert binary to decimal, digit by digit.
    */

    while (binary > 0) {
        rest = binary % 10;

        if (rest != 0 && rest != 1) {

            printf("Fehler bei der Eingabe.\n");
            return 255;
        }

        decimal += rest * base;
        binary /= 10;
        base *= 2;
    }

    printf("Dezimal: %d", decimal);

    return 0;
}
