/**
 * Read in a string with or without spaces and
 * print out whether the read-in string is a
 * palindrome, or not.
**/

#include <stdio.h>
#include <string.h> /* strlen   */
#include <ctype.h>  /* toupper  */

/*
Read in a string with the maximum length "length",
ending at "end" char; store the string in the buffer.

Returns the number of characters read, -1 if error encountered.
*/

int readString(char* buffer, int length, char end);

/*
Checks if given string is a palindrome.
*/

void isPalindrome(char* buffer);

int main(void) {

    char string[256];
    int i = 0;

    if (readString(string, 255, '\n') == -1) {

        printf("Fehler bei der Eingabe.\n");
        return 255;
    }

    while(string[i]) {
      string[i] = (toupper(string[i]));
      i++;
   }

    isPalindrome(string);
}

int readString(char* buffer, int length, char end) {

    int count = -1;

    for (char c = 0; c != end; c = getchar()) {
        
        if (count == length && c != end) {
            return -1;
        }
        
        buffer[count] = c;
        count++;
    }

    buffer[count] = '\0';

    return count;
}

void isPalindrome(char* buffer) {

    int left = 0, right = strlen(buffer) - 1;
    
    while (right > left) {

        if (buffer[left++] != buffer[right--]) {
            printf("kein Palindrom\n");
            return;
        }
    }

    printf("Palindrom\n");
    return;
}
