/**
 * Read in a number from StdIn and print out
 * its prime factors.
**/

#include <stdio.h>
#include <stdlib.h>     /* strtol   */
#include <math.h>       /* sqrt     */
#include <limits.h>     /* UINT_MAX */


/*
Print out the prime factors of a given number n.
*/

void primeFactors(unsigned int n);


int main(int argc, char** argv) {

    if (argc != 2) {

        printf("Falsche Argumentzahl\n");
        return 255;
    }

    char* ptr;
    long ret;
    *argv++;

    ret = strtol(*argv, &ptr, 10);

    if (ret > UINT_MAX) {
        printf("Fehler bei der Eingabe.\n");
        return 255;
    }

    printf("%d: ", (int)ret);

    primeFactors(ret);

    return 0;
}


void primeFactors(unsigned int n) {

    while (n%2 == 0) {
        printf("%d ", 2);
        n /= 2;
    }

    for (int i = 3; i <= sqrt(n); i += 2) {

        while (n % i == 0) {

            printf("%d ", i);
            n /= i;
        }
    }

    if (n > 2)
        printf("%d", n);
}
