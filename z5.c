/**
* Sort part of an array using selection sort
* and return the number of repositioned elements.
*
* Uses exclusively pointer arithmetics.
**/

/*
swap function, as used in S9
*/

void swap(int *left, int *right);

/*
Sort part of an array using selection sort.
*/

int sortieren(int *von, int *bis);

int sortieren(int *von, int *bis) {

	int *min, swapCount = 0;

	for (; von < bis; von++) {

		min = von;
		for (int* cur = von + 1; cur < bis; cur++)
			if (*cur < *min) {
				min = cur;
			}

		if (*min != *von) {

			swap(min, von);
			swapCount++;
		}
	}

	return swapCount;
}

void swap(int *left, int *right) {
	int tmp = *left;
	*left = *right;
	*right = tmp;
}
