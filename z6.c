/**
 * Implements the recursive function sum.
**/

int sum(int n);

int sum(int n) {

    if (n == 0)
        return n;
    
    if (n > 0)
        return sum(n - 1) + n;

    return sum(n + 1) + n;
}
