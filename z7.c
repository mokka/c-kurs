/**
 * Implements the functions strinv and strconcat.
**/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/*
Returns the inversion of string s.
*/

char *strinv(const char *s);

/*
Returns the concatenation of strings s and t.
*/

char *strconcat(const char *s, const char *t) {


char *strinv(const char *s) {

    const int len = strlen(s);

    char* str = malloc((len + 1) * sizeof(char));

    strncpy(str, s, len);

    char *p1, *p2;
    for (p1 = str, p2 = str + strlen(str) - 1; p2 > p1; ++p1, --p2)
    {
        *p1 ^= *p2;
        *p2 ^= *p1;
        *p1 ^= *p2;
    }

    return str;
}

char *strconcat(const char *s, const char *t) {

    const int lenS = strlen(s);
    const int lenT = strlen(t);
    
    char* str = malloc((lenS + lenT + 1) * sizeof(char));

    strcpy(str, s);
    strcat(str, t);

    return str;
}
