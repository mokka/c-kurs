/**
* Implements the struct bruch_t and some standard
* operations involving this struct.
**/

#include <stdio.h>

typedef struct Bruch {
	int zaehler;
	int nenner;
} bruch_t;

/*
Returns the GCD (greatest common divisor) of two numbers, util.
*/

int gcd(int a, int b);

/*
Returns the LCM (least common multiple) of two numbers, util.
*/

int lcm(int a, int b);

/*
Read in a fraction/bruch_t.
*/

void bruchEingabe(bruch_t* bruch);

/*
Print out a fraction/bruch_t
*/

void bruchAusgabe(bruch_t* bruch);

/*
Add one fraction to another.
*/

void bruchAddition(bruch_t* bruch, bruch_t add);

/*
Subtract one fraction from another one.
*/

void bruchSubtraktion(bruch_t* bruch, bruch_t sub);

/*
Multiply two fractions together.
*/

void bruchMultiplikation(bruch_t* bruch, bruch_t mult);

/*
Divide two fractions.
*/

void bruchDivision(bruch_t* bruch, bruch_t div);

/*
Returns the decimal value of the fraction.
*/

float bruchQuotient(bruch_t bruch);

/*
Cancel a fraction.
*/

void bruchKuerzen(bruch_t* bruch);

/*
Expand a fraction.
*/

void bruchErweitern(bruch_t* bruch, int faktor);

/*
============================
    End declaration block
============================
*/

void bruchEingabe(bruch_t* bruch) {

	/*
	Read in values for numerator and denominator from StdIn
	and update the values in passed bruch_t, bruch.
	*/

	int z, n;

	scanf("%d", &z);
	scanf("%d", &n);

	bruch->zaehler = z;
	bruch->nenner = n;
}

void bruchAusgabe(bruch_t* bruch) {

	printf("%d/%d\n", bruch->zaehler, bruch->nenner);
}

void bruchAddition(bruch_t* bruch, bruch_t add) {

	int tmp = bruch->nenner;

	bruchErweitern(bruch, add.nenner);
	bruchErweitern(&add, tmp);


	bruch->zaehler += add.zaehler;

	bruchKuerzen(bruch);
}

void bruchSubtraktion(bruch_t* bruch, bruch_t sub) {

	int tmp = bruch->nenner;

	bruchErweitern(bruch, sub.nenner);
	bruchErweitern(&sub, tmp);

	bruch->zaehler -= sub.zaehler;

	bruchKuerzen(bruch);
}

void bruchMultiplikation(bruch_t* bruch, bruch_t mult) {

	bruch->zaehler *= mult.zaehler;
	bruch->nenner *= mult.nenner;

	bruchKuerzen(bruch);
}

void bruchDivision(bruch_t* bruch, bruch_t div) {

	int tmp = div.nenner;
	div.nenner = div.zaehler;
	div.zaehler = tmp;

	bruchMultiplikation(bruch, div);
}

float bruchQuotient(bruch_t bruch) {

	return (((float)bruch.zaehler) / ((float)bruch.nenner));
}

void bruchKuerzen(bruch_t* bruch) {

	if (bruch->zaehler % bruch->nenner == 0) {
		bruch->zaehler /= bruch->nenner;
		bruch->nenner = 1;

		return;
	}

	int c = gcd(bruch->zaehler, bruch->nenner);

	bruch->zaehler /= c;
	bruch->nenner /= c;
}
void bruchErweitern(bruch_t* bruch, int faktor) {

	bruch->zaehler *= faktor;
	bruch->nenner *= faktor;
}


int gcd(int a, int b) {

	a = (a > 0) ? a : -a;
	b = (b > 0) ? b : -b;

	if (a == b)
		return a;

	if (a > b)
		return gcd(a - b, b);

	return gcd(a, b - a);
}

int lcm(int a, int b) {

	return (a * b) / gcd(a, b);
}
